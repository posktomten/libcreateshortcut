//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CREATESHORTCUT (Linux)
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcreateshortcut
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "createshortcut_lin.h"
#ifndef QT_STATIC
#include "createshortcut_lin_global.h"
#endif

void Createshortcut::removeApplicationShortcut(QString *executable_name)
{
    const QString applicationPath = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation) + "/" + *executable_name + QStringLiteral(".desktop");
    QFile file(applicationPath);

    if(file.exists()) {
        if(file.remove()) {
            return;
        } else {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(*executable_name);
            msgBox.setText("\"" + applicationPath + QStringLiteral("\"") + tr(" can not be removed.<br>Pleas check your file permissions."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }
    }
}

void Createshortcut::removeDesktopShortcut(QString *executable_name)
{
    const QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + "/" + *executable_name + QStringLiteral(".desktop");
    QFile file(desktopPath);

    if(file.exists()) {
        if(file.remove()) {
            return;
        } else {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(*executable_name);
            msgBox.setText("\"" + desktopPath + QStringLiteral("\"") + tr(" can not be removed.<br>Pleas check your file permissions."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }
    }
}

