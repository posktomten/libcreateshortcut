<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ln_CD">
<context>
    <name>Createshortcut</name>
    <message>
        <location filename="../createshortcut.cpp" line="36"/>
        <source>not found.&lt;br&gt;The shortcut will be created without an icon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="37"/>
        <location filename="../createshortcut.cpp" line="48"/>
        <location filename="../createshortcut.cpp" line="110"/>
        <location filename="../deleteallsettings.cpp" line="49"/>
        <location filename="../deleteallsettings.cpp" line="63"/>
        <location filename="../deleteallsettings.cpp" line="78"/>
        <location filename="../removeshortcut.cpp" line="37"/>
        <location filename="../removeshortcut.cpp" line="57"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="47"/>
        <source>not found.&lt;br&gt;The shortcut will not be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="109"/>
        <source>The shortcut could not be created.&lt;br&gt;Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="26"/>
        <source>Warning!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="27"/>
        <source>All your saved settings will be deleted.
All shortcuts will be removed.
And the program will exit.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="30"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="31"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="47"/>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="62"/>
        <location filename="../deleteallsettings.cpp" line="77"/>
        <location filename="../removeshortcut.cpp" line="36"/>
        <location filename="../removeshortcut.cpp" line="56"/>
        <source> can not be removed.&lt;br&gt;Pleas check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
