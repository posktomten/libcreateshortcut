<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Createshortcut</name>
    <message>
        <source>not found.&lt;br&gt;The shortcut will be created without an icon.</source>
        <translation>hittas inte.&lt;br&gt;Genvägen kommer att skapas utan ikon.</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>not found.&lt;br&gt;The shortcut will not be created</source>
        <translation>hittas inte.&lt;br&gt;Genvägen kommer inte att skapas</translation>
    </message>
    <message>
        <source>The shortcut could not be created.&lt;br&gt;Check your file permissions.</source>
        <translation>Genvägen kunde inte skapas.&lt;br&gt;Kolla dina filrättigheter.</translation>
    </message>
    <message>
        <source>Warning!</source>
        <translation>Varning!</translation>
    </message>
    <message>
        <source>All your saved settings will be deleted.
All shortcuts will be removed.
And the program will exit.
Do you want to continue?</source>
        <translation>Alla dina sparade inställningar kommer att raderas.
Alla genvägar kommer att tas bort.
Och programmet kommer att avslutas.
Vill du fortsätta?</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation>Det gick inte att ta bort dina konfigurationsfiler.
Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <source> can not be removed.&lt;br&gt;Pleas check your file permissions.</source>
        <translation> kan inte tas bort.&lt;br&gt;Vänligen kontrollera dina filrättigheter.</translation>
    </message>
</context>
</TS>
