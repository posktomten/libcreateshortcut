//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CREATESHORTCUT (Linux)
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcreateshortcut
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "createshortcut_lin.h"

void Createshortcut::makeShortcutFile(QString *display_name, QString *executable_name, QString *comments, QString *categories, QString *icon, bool applications, bool desktop)
{
#ifdef APPIMAGE
    QString source = qgetenv("APPIMAGE");
#endif
#ifndef APPIMAGE
    const QString source = QCoreApplication::applicationDirPath() + "/" + *executable_name;
#endif
    const QString pngpath = QCoreApplication::applicationDirPath() + "/" + *icon;
    QFileInfo fi(pngpath);

    if(!fi.exists()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(*display_name);
        msgBox.setText("\"" + pngpath + "\"<br>" + tr("not found.<br>The shortcut will be created without an icon."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
    }

    fi.setFile(source);

    if(!fi.exists()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(*display_name);
        msgBox.setText(QStringLiteral("\"") + source + QStringLiteral("\"<br>") + tr("not found.<br>The shortcut will not be created"));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, *display_name,
                       *executable_name);
    settings.beginGroup("Settings");
    QString inifile = settings.fileName();
    fi.setFile(inifile);
    const QString iconpath = fi.absolutePath() + QStringLiteral("/") + *icon;
    QFile file(pngpath);

    if(!QFile::exists(iconpath)) {
        if(QFile::copy(pngpath, iconpath)) {
            file.setPermissions(QFileDevice::ReadUser | QFileDevice::WriteUser | QFileDevice::ReadOther);
        }
    }

    const QString applicationPath = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation) + "/" + *executable_name + QStringLiteral(".desktop");
    const QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + "/" + *executable_name + QStringLiteral(".desktop");

    if(applications) {
        QFile applicationFile(applicationPath);

        if(applicationFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream out(&applicationFile);

            if(QFile::exists(iconpath)) {
                out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=" + *display_name + "\nIcon=" + iconpath + "\nTerminal=false\nComment=" + *comments + "\nExec=\"" + source + "\"\nCategories=" + *categories + ";\n";
            } else {
                out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName" + *display_name + "\nTerminal=false\nComment=" + *comments + "\nExec=\"" + source + "\"\nCategories=" + *categories + ";\n";
            }

            applicationFile.close();
            applicationFile.setPermissions(QFileDevice::ReadUser | QFileDevice::ExeUser |
                                           QFileDevice::WriteUser | QFileDevice::ReadOther |
                                           QFileDevice::ExeOther);
        }
    }

    if(desktop) {
        QFile desktopFile(desktopPath);

        if(desktopFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream out(&desktopFile);

            if(QFile::exists(iconpath)) {
                out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=" + *display_name + "\nIcon=" + iconpath + "\nTerminal=false\nComment=" + *comments + "\nExec=\"" + source + "\"\nCategories=" + *categories + ";\n";
            } else {
                out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName" + *display_name + "\nTerminal=false\nComment=" + *comments + "\nExec=\"" + source + "\"\nCategories=" + *categories + ";\n";
            }

            desktopFile.close();
            desktopFile.setPermissions(QFileDevice::ReadUser | QFileDevice::ExeUser |
                                       QFileDevice::WriteUser | QFileDevice::ReadOther |
                                       QFileDevice::ExeOther);
        } else {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(*display_name);
            msgBox.setText(tr("The shortcut could not be created.<br>Check your file permissions."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }
    }
}
