//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CREATESHORTCUT (Linux)
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcreateshortcut
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "createshortcut_lin.h"
#include <QMessageBox>
#include <QPushButton>
#include <QDir>
bool Createshortcut::deleteAllSettings(QString *display_name, QString *executable_name, QString *version, QIcon *icon)
{
    QMessageBox *msgBox = new QMessageBox;
    msgBox->setWindowIcon(*icon);
    msgBox->setText(QStringLiteral("<b>") + tr("Warning!") + QStringLiteral("</b>"));
    msgBox->setInformativeText(tr("All your saved settings will be deleted.\nAll shortcuts will be removed.\nAnd the program will exit.\nDo you want to continue?"));
    msgBox->setIcon(QMessageBox::Warning);
    msgBox->setWindowTitle(*display_name + " " + *version);
    QPushButton  *rejectButton = new QPushButton(tr("No"), msgBox);
    QPushButton  *acceptButton = new QPushButton(tr("Yes"), msgBox);
    msgBox->addButton(acceptButton, QMessageBox::YesRole);
    msgBox->addButton(rejectButton, QMessageBox::RejectRole);
    msgBox->setDefaultButton(rejectButton);
    msgBox->exec();

    if(msgBox->clickedButton() == acceptButton) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, *display_name,
                           *executable_name);
        QFileInfo fi(settings.fileName());
        QDir *dir2 = new QDir(fi.absolutePath());

        if(!dir2->removeRecursively()) {
            QMessageBox *msgBox = new QMessageBox;
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->setWindowTitle(*display_name + " " + *version);
            msgBox->setText(tr("Failed to delete your configuration files.\n"
                               "Check your file permissions."));
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->exec();
            return false;
        }

        const QString applicationPath = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation) + "/" + *executable_name + ".desktop";

        /***/
        if(QFile::exists(applicationPath)) {
            if(!QFile::remove(applicationPath)) {
                QMessageBox *msgBox = new QMessageBox;
                msgBox->setIcon(QMessageBox::Critical);
                msgBox->setWindowTitle(*executable_name);
                msgBox->setText("\"" + applicationPath + "\"" + tr(" can not be removed.<br>Pleas check your file permissions."));
                msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox->exec();
                return false;
            }
        }

        /**/
        QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + "/" + *executable_name + ".desktop";

        if(QFile::exists(desktopPath)) {
            if(!QFile::remove(desktopPath)) {
                QMessageBox *msgBox = new QMessageBox;
                msgBox->setIcon(QMessageBox::Critical);
                msgBox->setWindowTitle(*executable_name);
                msgBox->setText("\"" + desktopPath + "\"" + tr(" can not be removed.<br>Pleas check your file permissions."));
                msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox->exec();
                return false;
            }
        }

        return true;
    }

    return false;
}
